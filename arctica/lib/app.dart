import 'package:arctica/data/arctica_data_source.dart';
import 'package:arctica/models/space_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  List<SpaceModel> spaces = [];
  @override
  void initState() {
    getSpace();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(left: 12, right: 12),
        child: ListView.builder(
            itemCount: spaces.length,
            itemBuilder: (context, index) {
              return SpaceCard(model: spaces[index]);
            }),
      ),
    );
  }

  void getSpace() async {
    spaces = await ArcticaDataSource.getSpaces();
    setState(() {});
  }
}

class SpaceCard extends StatelessWidget {
  const SpaceCard({Key? key, required this.model}) : super(key: key);

  final SpaceModel model;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8, bottom: 8),
      child: Container(
        decoration: BoxDecoration(
            color: Color(0xffF2F2F3), borderRadius: BorderRadius.circular(12)),
        child: Padding(
          padding:
              const EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 16),
          child: Column(children: [
            Row(children: [
              CircleAvatar(
                radius: 36,
                backgroundImage: Image.network(
                  model.image,
                ).image,
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      model.title,
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SvgPicture.asset('assets/icons/Icon.svg'),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                          child: Text(
                            model.addres,
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: Color.fromARGB(255, 51, 51, 51)),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ]),
            SizedBox(
              height: 12,
            ),
            Text(
              model.caption,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            )
          ]),
        ),
      ),
    );
  }
}
