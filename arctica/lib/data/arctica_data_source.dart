import 'package:dio/dio.dart';

import '../models/space_model.dart';

const token = 'Вставь токен';

class ArcticaDataSource {
  static final _client = Dio();
  static Future<List<SpaceModel>> getSpaces() async {
    final headers = {'Authorization': 'Bearer $token'};

    final respons = await _client.get(
        'https://arctica.qubeek.io/api/content/spaces?page=1',
        options: Options(headers: headers));
    final json = respons.data['resources'] as List;

    final spaces = json.map((e) => SpaceModel.fromJson(e)).toList();
    return spaces;
  }
}
