import 'package:flutter/cupertino.dart';

class SpaceModel {
  final String title;
  final String caption;
  final int id;
  final String image;
  final String addres;

  SpaceModel({
    required this.title,
    required this.caption,
    required this.id,
    required this.image,
    required this.addres,
  });
  factory SpaceModel.fromJson(Map<String, dynamic> json) {
    final title = json['title'];
    final caption = json['caption'];
    final id = json['id'];
    final listMedia = json['media'] as List;
    final jsonMedia = listMedia.first;
    final image = jsonMedia['path'];
    final address = json['address']["title"];

    return SpaceModel(
        title: title, caption: caption, id: id, image: image, addres: address);
  }
}
