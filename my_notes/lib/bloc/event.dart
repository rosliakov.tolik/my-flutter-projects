import 'package:my_notes/models/notes_model.dart';

abstract class NotesEvent {}

// Пойти в базу данных и достать все заметки
class ReadNotesEvent extends NotesEvent {}

// Поиск заметок по слову
class SearchNotesEvent extends NotesEvent {
  final String query;

  SearchNotesEvent(this.query);
}

// Добавить заметку
class AddNotesEvent extends NotesEvent {
  final NotesModel note;
  AddNotesEvent({
    required this.note,
  });
}

// Удалить замтку
class DeleteNotesEvent extends NotesEvent {
  final int id;

  DeleteNotesEvent(this.id);
}

class EditNotesEvent extends NotesEvent {
  final NotesModel note;

  EditNotesEvent(this.note);
}
