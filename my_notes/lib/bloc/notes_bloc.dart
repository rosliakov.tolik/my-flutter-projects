import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_notes/bloc/event.dart';
import 'package:my_notes/bloc/state.dart';
import 'package:my_notes/repository/notes_repository.dart';
import 'package:my_notes/repository/notes_repository_sql.dart';

class NotesBloc extends Bloc<NotesEvent, NotesState> {
  NotesBloc(this._repository) : super(EmptyNotesState()) {
    on<SearchNotesEvent>(
      (event, emitter) async {
        final notes = await _repository.searchNotes(event.query);
        emitter(LoadedNotestState(notes));
      },
    );
    on<EditNotesEvent>(
      (event, emitter) async {
        await _repository.updateNotes(event.note);
        add(ReadNotesEvent());
      },
    );
    on<AddNotesEvent>(
      (event, emitter) async {
        final note = event.note;
        await _repository.addNotes(note);
        add(ReadNotesEvent());
      },
    );
    on<DeleteNotesEvent>(
      (event, emitter) async {
        await NotesRepositorySQL().deleteNotes(event.id);
        add(ReadNotesEvent());
      },
    );
    on<ReadNotesEvent>(
      (event, emitter) async {
        emitter(InProccecingNotesState());
        final notes = await _repository.getAllNotes();
        if (notes.isNotEmpty) {
          emitter(LoadedNotestState(notes));
        } else {
          emitter(EmptyNotesState());
        }
      },
    );
  }

  final NotesRepository _repository;
}
