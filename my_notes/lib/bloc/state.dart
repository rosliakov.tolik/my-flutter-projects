import 'package:my_notes/models/notes_model.dart';

abstract class NotesState {}

// В процессе получения данных
class InProccecingNotesState extends NotesState {}

// Данные получены
class LoadedNotestState extends NotesState {
  final List<NotesModel> notes;
  LoadedNotestState(this.notes);
}

// База данных пустая
class EmptyNotesState extends NotesState {}

// Ошибка
class FailureNotestState extends NotesState {}
