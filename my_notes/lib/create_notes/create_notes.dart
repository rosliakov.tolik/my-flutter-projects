import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_notes/bloc/event.dart';
import 'package:my_notes/bloc/notes_bloc.dart';
import 'package:my_notes/models/notes_model.dart';
import 'package:my_notes/repository/notes_repository_sql.dart';

class CreateNotes extends StatelessWidget {
  CreateNotes({Key? key, required this.onBack}) : super(key: key);
  final Function() onBack;

  TextEditingController teTitle = TextEditingController();
  TextEditingController teDescription = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
              onBack.call();
            },
          ),
          backgroundColor: Colors.amber,
          centerTitle: true,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(10),
            ),
          )),
      body: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 10, left: 8, right: 8),
            child: TextField(
              controller: teTitle,
              decoration: const InputDecoration(
                labelText: 'Заголовок',
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12))),
              ),
            ),
          ),
          Container(
            width: double.infinity,
            margin: const EdgeInsets.only(top: 10, left: 8, right: 8),
            child: TextField(
              maxLines: 5,
              minLines: 1,
              controller: teDescription,
              decoration: const InputDecoration(
                labelText: 'Описание',
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12))),
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: 50,
            margin: const EdgeInsets.all(15),
            child: ElevatedButton(
                style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ))),
                onPressed: () {
                  final note = NotesModel(
                      id: 1,
                      title: teTitle.text,
                      description: teDescription.text);
                  context.read<NotesBloc>().add(AddNotesEvent(note: note));
                  Navigator.pop(context);
                },
                child: const Text(
                  'Сохранить',
                  style: TextStyle(color: Colors.black54),
                )),
          )
        ],
      ),
    );
  }
}
