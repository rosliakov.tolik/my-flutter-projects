import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'models/dbnotes.dart';

class DBProvider {
  static final DBProvider db = DBProvider();
  Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await initDB();
    return _database!;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'TestDB.db');
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute(
          'CREATE TABLE MY_LIST (id INTEGER PRIMARY KEY, title TEXT, description TEXT)');
    });
  }

  addNotes(DBNotes notes) async {
    final db = await database;
    var raw = await db.rawInsert(
      'INSERT Into MY_LIST (id, title, description)'
      'VALUES (?,?,?)',
      [notes.id, notes.title, notes.description],
    );
    return raw;
  }

  Future<List<DBNotes>> getAllNotes() async {
    final db = await database;
    final res = await db.query('MY_LIST');
    List<DBNotes> list =
        res.isNotEmpty ? res.map((e) => DBNotes.fromMap(e)).toList() : [];
    return list;
  }

  deleteNotesId(int id) async {
    final db = await database;
    return db.delete('MY_LIST', where: 'id = ?', whereArgs: [id]);
  }

  deleteAllTasks() async {
    final db = await database;
    return db.rawDelete('Delete FROM MY_LIST');
  }

  updataNotes(DBNotes notes) async {
    final db = await database;
    var res = await db.update(
      'MY_LIST',
      notes.toMap(),
      where: 'id = ?',
      whereArgs: [notes.id],
    );
    return res;
  }

  Future<List<DBNotes>> searchNotes(String keyword) async {
    final db = await database;
    List<Map<String, dynamic>> result =
        await db.rawQuery('SELECT * FROM MY_LIST WHERE title=?', [keyword]);
    List<DBNotes> notes =
        result.map((notes) => DBNotes.fromMap(notes)).toList();
    return notes;
  }
}
