import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_notes/bloc/event.dart';
import 'package:my_notes/bloc/notes_bloc.dart';
import 'package:my_notes/models/notes_model.dart';

import '../repository/notes_repository_sql.dart';

class EditNotes extends StatelessWidget {
  final NotesModel notesModel;
  const EditNotes({Key? key, required this.notesModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController teTitle =
        TextEditingController(text: notesModel.title);
    TextEditingController teDescription =
        TextEditingController(text: notesModel.description);
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Column(children: [
          Container(
            margin: const EdgeInsets.only(
              top: 10,
              left: 15,
              right: 15,
            ),
            child: TextField(
              controller: teTitle,
              decoration: const InputDecoration(
                hintText: 'Заголовок',
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8))),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
              top: 10,
              left: 15,
              right: 15,
            ),
            child: TextField(
              maxLines: null,
              controller: teDescription,
              decoration: const InputDecoration(
                hintText: 'Описание',
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8))),
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: 60,
            decoration: const BoxDecoration(
                color: Colors.amber,
                borderRadius: BorderRadius.all(Radius.circular(8))),
            margin: const EdgeInsets.all(15),
            child: ElevatedButton(
                onPressed: () {
                  final notes = NotesModel(
                      id: notesModel.id,
                      title: teTitle.text,
                      description: teDescription.text);
                  context.read<NotesBloc>().add(EditNotesEvent(notes));
                  Navigator.pop(context);
                },
                child: const Text('Сохранить')),
          )
        ]),
      ),
    );
  }
}
