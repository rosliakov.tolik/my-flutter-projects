import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_notes/bloc/event.dart';
import 'package:my_notes/bloc/notes_bloc.dart';
import 'package:my_notes/bloc/state.dart';
import 'package:my_notes/create_notes/create_notes.dart';
import 'package:my_notes/edit_notes/edit_notes.dart';
import 'package:my_notes/models/notes_model.dart';

import 'repository/notes_repository_sql.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController keyword = TextEditingController();

  PreferredSizeWidget _buildAppBAr() {
    return AppBar(
      backgroundColor: Colors.amber,
      centerTitle: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(10),
        ),
      ),
      title: Container(
          height: 40,
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(15)),
          child: Center(
              child: TextField(
            onChanged: (s) {
              context.read<NotesBloc>().add(SearchNotesEvent(s));
            },
            controller: keyword,
            onSubmitted: ((value) {
              setState(() {
                // ignore: unrelated_type_equality_checks
                keyword == value;
              });
            }),
            decoration: InputDecoration(
              prefixIcon: IconButton(
                icon: const Icon(Icons.search),
                onPressed: () {
                  setState(() {
                    // NotesRepositorySQL().searchNotes(keyword.text);
                    context
                        .read<NotesBloc>()
                        .add(SearchNotesEvent(keyword.text));
                  });
                },
              ),
              suffixIcon: IconButton(
                icon: const Icon(
                  Icons.clear,
                ),
                onPressed: () {
                  setState(() {
                    keyword.text = '';
                  });
                  context.read<NotesBloc>().add(ReadNotesEvent());
                },
              ),
              hintText: 'Поиск...',
              border: InputBorder.none,
            ),
          ))),
      actions: [
        IconButton(
          onPressed: () {
            setState(() {
              NotesRepositorySQL().deleteAllTasks();
            });
          },
          icon: const Icon(
            Icons.delete,
            color: Colors.black54,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBAr(),
      body: BlocBuilder<NotesBloc, NotesState>(
        builder: ((context, state) {
          if (state is InProccecingNotesState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is EmptyNotesState) {
            return const Center(
              child: Text('Заметок нет'),
            );
          } else if (state is LoadedNotestState) {
            final notes = state.notes;
            return ListView.builder(
                itemCount: notes.length,
                itemBuilder: (context, index) {
                  return NotesCard(note: notes[index]);
                });
          }
          return const Center(
            child: Text('Произошла ошибка'),
          );
        }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CreateNotes(
                        onBack: () {
                          setState(() {});
                        },
                      )));
        },
        child: Icon(
          Icons.create,
          color: Colors.black54,
        ),
      ),
    );
  }
}

class NotesCard extends StatefulWidget {
  NotesCard({Key? key, required this.note}) : super(key: key);
  final NotesModel note;

  @override
  State<NotesCard> createState() => _NotesCardState();
}

class _NotesCardState extends State<NotesCard> {
  bool more = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:
          BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(12))),
      margin: const EdgeInsets.only(top: 8),
      child: Column(
        children: [
          GestureDetector(
            // press to card
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => EditNotes(
                            notesModel: widget.note,
                          )));
            },
            child: Container(
              margin: EdgeInsets.only(left: 8, right: 8),
              child: Card(
                shadowColor: Colors.amber,
                elevation: 5.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12))),
                child: ListTile(
                  title: Text(
                    widget.note.title,
                  ),
                  trailing: IconButton(
                    // press to delet
                    onPressed: () {
                      context.read<NotesBloc>().add(DeleteNotesEvent(
                            widget.note.id,
                          ));
                    },
                    icon: const Icon(
                      Icons.delete_outline,
                      color: Colors.black54,
                    ),
                  ),
                  subtitle: widget.note.description.length > 22
                      ? AnimatedSize(
                          duration: Duration(milliseconds: 200),
                          child: InkWell(
                            child: Text(
                              more
                                  ? widget.note.description
                                  : widget.note.description.substring(0, 22),
                              style: more
                                  ? null
                                  : TextStyle(
                                      decoration: TextDecoration.underline,
                                    ),
                            ),
                            onTap: () {
                              setState(() {
                                more = !more;
                              });
                            },
                          ),
                        )
                      : Text(
                          widget.note.description,
                        ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
