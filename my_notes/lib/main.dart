import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_notes/bloc/event.dart';
import 'package:my_notes/bloc/notes_bloc.dart';
import 'package:my_notes/home_screen.dart';
import 'package:my_notes/repository/notes_repository_sql.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: ((context) =>
          NotesBloc(NotesRepositorySQL())..add(ReadNotesEvent())),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primarySwatch: Colors.amber),
        home: const HomeScreen(),
      ),
    );
  }
}
