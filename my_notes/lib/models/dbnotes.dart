import 'dart:convert';

import 'package:flutter/cupertino.dart';

class DBNotes {
  int id;
  String title;
  String description;

  DBNotes({
    this.id = 0,
    this.title = '',
    this.description = '',
  });

  factory DBNotes.fromMap(Map<String, dynamic> json) => DBNotes(
        id: json['id'] ?? 0,
        title: json['title'] ?? '',
        description: json['description'] ?? '',
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'title': title,
        'description': description,
      };
}
