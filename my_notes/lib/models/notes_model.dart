class NotesModel {
  final int id;
  final String title;
  final String description;

  NotesModel({
    required this.id,
    required this.title,
    required this.description,
  });

  factory NotesModel.fromMap(Map<String, dynamic> json) => NotesModel(
        id: json['id'] ?? 0,
        title: json['title'] ?? '',
        description: json['description'] ?? '',
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'title': title,
        'description': description,
      };
}
