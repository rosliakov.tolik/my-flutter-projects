import 'package:my_notes/models/notes_model.dart';

abstract class NotesRepository {
  Future<void> addNotes(NotesModel notes);

  Future<List<NotesModel>> getAllNotes();

  Future updateNotes(NotesModel notes);

  Future<List<NotesModel>> searchNotes(String keyword);

  Future<void> deleteNotes(int id);

  Future<void> deleteAllTasks();
}
