import 'package:my_notes/database.dart';

import '../models/dbnotes.dart';
import '../models/notes_model.dart';
import 'notes_repository.dart';

class NotesRepositorySQL extends NotesRepository {
  @override
  Future<void> addNotes(NotesModel notes) async {
    final getAllNotesD = await getAllNotes();
    await DBProvider.db.addNotes(DBNotes(
      id: getAllNotesD.length == 0 ? 1 : getAllNotesD.last.id + notes.id,
      title: notes.title,
      description: notes.description,
    ));
  }

  @override
  Future<List<NotesModel>> getAllNotes() async {
    final tt = await DBProvider.db.getAllNotes();
    return tt
        .map((e) => NotesModel(
              id: e.id,
              title: e.title,
              description: e.description,
            ))
        .toList();
  }

  @override
  Future updateNotes(NotesModel notes) async {
    final result = await DBProvider.db.updataNotes(DBNotes(
      id: notes.id,
      title: notes.title,
      description: notes.description,
    ));
    return result;
  }

  @override
  Future<List<NotesModel>> searchNotes(String keyword) async {
    // List<NotesModel> _searchList = [];

    final tt = (await DBProvider.db.getAllNotes())
        .map((e) => NotesModel(
              id: e.id,
              title: e.title,
              description: e.description,
            ))
        .toList();

    tt.removeWhere((element) =>
        !element.title.toLowerCase().contains(keyword.toLowerCase()));

    return tt;

    // final tt = await DBProvider.db.searchNotes(keyword);
    // return tt
    //     .map((e) => NotesModel(
    //           id: e.id,
    //           title: e.title,
    //           description: e.description,
    //         ))
    //     .toList();
  }

  @override
  Future<void> deleteNotes(int id) async {
    await DBProvider.db.deleteNotesId(id);
  }

  @override
  Future<void> deleteAllTasks() async {
    await DBProvider.db.deleteAllTasks();
  }
}
