import 'package:flutter/material.dart';

class TextFormTime extends StatelessWidget {
  const TextFormTime({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        maxLines: 2,
        minLines: 1,
        maxLength: 60,
        keyboardType: TextInputType.text,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
        decoration: const InputDecoration(
            labelText: 'Задача',
            labelStyle: TextStyle(color: Colors.white),
            hintText: 'Кратко опиши задачу',
            hintStyle: TextStyle(color: Color.fromARGB(255, 218, 218, 218)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(12)),
                borderSide: BorderSide(color: Colors.white)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(12)),
                borderSide: BorderSide(color: Colors.white))),
      ),
    );
  }
}
