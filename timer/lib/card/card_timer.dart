import 'dart:async';

import 'package:flutter/material.dart';

import 'card_field.dart';

class CardTimer extends StatefulWidget {
  const CardTimer({Key? key}) : super(key: key);

  @override
  State<CardTimer> createState() => _CardTimerState();
}

class _CardTimerState extends State<CardTimer> {
  int seconds = 0, minutes = 0, hours = 0;
  String digitsSecond = '00', digitsMinutes = '00', digitsHours = '00';
  Timer? timer;
  bool started = false;
  List laps = [];

  void stop() {
    timer!.cancel();
    setState(() {
      started = false;
    });
  }

  void reset() {
    timer!.cancel();
    setState(() {
      seconds = 0;
      minutes = 0;
      hours = 0;

      digitsSecond = '00';
      digitsMinutes = '00';
      digitsHours = '00';

      started = false;
    });
  }

  void start() {
    started = true;
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      int localSecond = seconds + 1;
      int localMinutes = minutes;
      int localHours = hours;

      if (localSecond > 59) {
        if (localMinutes > 59) {
          localHours++;
          localMinutes = 0;
        } else {
          localMinutes++;
          localSecond = 0;
        }
      }
      setState(() {
        seconds = localSecond;
        minutes = localMinutes;
        hours = localHours;
        digitsSecond = (seconds >= 10) ? "$seconds" : "0$seconds";
        digitsMinutes = (minutes >= 10) ? "$minutes" : "0$minutes";
        digitsHours = (hours >= 10) ? "$hours" : "0$hours";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 180,
        margin: const EdgeInsets.only(top: 10, left: 8, right: 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: Theme.of(context).backgroundColor,
        ),
        child: Center(
            child: Column(
          children: [TextFormTime(), timerForm()],
        )));
  }

  Padding timerForm() {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
              onPressed: () {
                (!started) ? start() : stop();
              },
              icon: Icon((!started) ? Icons.play_arrow : Icons.pause),
              color: Colors.white,
            ),
            Text(
              '$digitsHours:$digitsMinutes:$digitsSecond',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w300,
                  fontSize: 24),
            ),
            IconButton(
                onPressed: () {
                  reset();
                },
                icon: Icon(Icons.replay, color: Colors.white))
          ],
        ));
  }
}
