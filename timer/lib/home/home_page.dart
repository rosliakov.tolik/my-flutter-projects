import 'package:flutter/material.dart';
import '../card/card_timer.dart';
import '../theme/config.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Widget> _cardList = [];

  void _addCardWidget() {
    setState(() {
      _cardList.add(CardTimer());
    });
  }

  void _deleteCardWidget() {
    setState(() {
      _cardList.removeLast();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Трекер времени',
            style: TextStyle(fontWeight: FontWeight.w300),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              onPressed: _deleteCardWidget,
              icon: Icon(Icons.delete),
            )
          ],
        ),
        drawer: Drawer(
          backgroundColor: Theme.of(context).dialogBackgroundColor,
          child: Column(
            children: [
              Container(
                color: Theme.of(context).backgroundColor,
                height: 150,
                child: Center(
                  child: ListTile(
                    title: Text(
                      'Изменить тему приложения',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w300),
                    ),
                    trailing: IconButton(
                      icon: const Icon(Icons.brightness_4),
                      onPressed: () => currentTheme.toggleTheme(),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        body: ListView.builder(
            itemCount: _cardList.length,
            itemBuilder: (context, index) {
              return _cardList[index];
            }),
        floatingActionButton: FloatingActionButton(
          onPressed: _addCardWidget,
          tooltip: 'Add',
          child: const Icon(Icons.add),
        ));
  }
}
