import 'package:flutter/material.dart';
import '../theme/config.dart';
import '../theme/custom_theme.dart';
import 'home_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialMyApp();
  }
}

class MaterialMyApp extends StatefulWidget {
  const MaterialMyApp({Key? key}) : super(key: key);
  @override
  State<MaterialMyApp> createState() => _MaterialMyAppState();
}

class _MaterialMyAppState extends State<MaterialMyApp> {
  @override
  void initState() {
    super.initState();
    currentTheme.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: CustomTheme.lightTheme,
      darkTheme: CustomTheme.darkTheme,
      themeMode: currentTheme.currentTheme,
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
