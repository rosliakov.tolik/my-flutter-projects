import 'package:flutter/material.dart';

class CustomTheme with ChangeNotifier {
  static bool _isDarkTheme = false;
  ThemeMode get currentTheme => _isDarkTheme ? ThemeMode.dark : ThemeMode.light;

  void toggleTheme() {
    _isDarkTheme = !_isDarkTheme;
    notifyListeners();
  }

  static Color lDark = Color.fromARGB(255, 88, 88, 88);
  static Color bDark = Color.fromARGB(115, 82, 82, 82);
  static Color lLight = Colors.white;
  static Color bLight = Colors.blue;
  static ThemeData get darkTheme {
    return ThemeData(
      scaffoldBackgroundColor: bDark,
      appBarTheme: AppBarTheme(color: lDark),
      buttonTheme: ButtonThemeData(buttonColor: lDark),
      floatingActionButtonTheme:
          FloatingActionButtonThemeData(backgroundColor: lDark),
      backgroundColor: lDark,
      dialogBackgroundColor: bDark,
      hintColor: Colors.grey,
    );
  }

  static ThemeData get lightTheme {
    return ThemeData(
      scaffoldBackgroundColor: lLight,
      appBarTheme: AppBarTheme(color: bLight),
      buttonTheme: ButtonThemeData(buttonColor: bLight),
      floatingActionButtonTheme:
          FloatingActionButtonThemeData(backgroundColor: bLight),
      backgroundColor: bLight,
      hintColor: Colors.white,
      dialogBackgroundColor: Color.fromARGB(178, 255, 255, 255),
    );
  }
}
